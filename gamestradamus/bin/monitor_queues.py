"""A Cron task that checks the celery worker queue length."""

import smtplib
import subprocess
from email.mime.text import MIMEText
from gamestradamus import cli

#-------------------------------------------------------------------------------
def check_queue(server, config, name='plebs', limit=200):
    """Check the length of a queue and if the limit is exceeded, notify the error address."""

    rabbit_process = subprocess.Popen(
        ["/usr/sbin/rabbitmqctl", "list_queues"],
        stdout=subprocess.PIPE,
    )
    grep_process = subprocess.Popen(
        ["/bin/grep", "-r", "%s" % name],
        stdin=rabbit_process.stdout,
        stdout=subprocess.PIPE,
    )
    awk_process = subprocess.Popen(
        ["/usr/bin/awk", "{ print $2 }"],
        stdin=grep_process.stdout,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    output, error = awk_process.communicate()
    try:
        count = int(output)
        if count > limit:
            msg = MIMEText('The "%s" queue has exceeded %s items. There are %s items waiting to be processed.\r\n\r\n\r\nGauge' % (name, limit, count))
            msg['Subject'] = '%s Queue Alert' % name
            msg['From'] = config['gauge.email.server_address']
            msg['To'] = config['gauge.email.error_address']

            server.sendmail(
                config['gauge.email.server_address'],
                config['gauge.email.error_address'],
                msg.as_string()
            )

    except (ValueError, TypeError), e:
        pass

#-------------------------------------------------------------------------------
if __name__ == '__main__':
    config = cli.get_global_config()

    server = smtplib.SMTP('smtp.gmail.com:587')
    server.starttls()
    server.login(
        config['gauge.email.server_address'],
        config['gauge.email.password']
    )
    check_queue(server, config, 'plebs', 1000)
    check_queue(server, config, 'premium', 100)
    check_queue(server, config, 'welcome', 50)
    server.quit()

